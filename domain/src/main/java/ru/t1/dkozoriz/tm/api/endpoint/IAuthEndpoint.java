package ru.t1.dkozoriz.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.dkozoriz.tm.dto.response.user.LoginUserResponse;
import ru.t1.dkozoriz.tm.dto.response.user.LogoutUserResponse;
import ru.t1.dkozoriz.tm.dto.response.user.ViewProfileUserResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    LoginUserResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    );

    @NotNull
    @WebMethod
    LogoutUserResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    );

    @NotNull
    @WebMethod
    ViewProfileUserResponse getProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserViewProfileRequest request
    );

}