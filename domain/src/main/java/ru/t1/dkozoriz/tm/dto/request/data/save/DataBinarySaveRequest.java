package ru.t1.dkozoriz.tm.dto.request.data.save;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

public class DataBinarySaveRequest extends AbstractUserRequest {

    public DataBinarySaveRequest(@Nullable final String token) {
        super(token);
    }

}