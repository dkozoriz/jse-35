package ru.t1.dkozoriz.tm.dto.request.data.save;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

public class DataBase64SaveRequest extends AbstractUserRequest {

    public DataBase64SaveRequest(@Nullable final String token) {
        super(token);
    }

}