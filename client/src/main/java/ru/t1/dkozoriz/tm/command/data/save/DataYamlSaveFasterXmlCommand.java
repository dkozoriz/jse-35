package ru.t1.dkozoriz.tm.command.data.save;

import lombok.SneakyThrows;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.request.data.save.DataBase64SaveRequest;
import ru.t1.dkozoriz.tm.dto.request.data.save.DataYamlSaveFasterXmlRequest;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    public DataYamlSaveFasterXmlCommand() {
        super("data-save-yaml", "save data in yaml file.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE YAML]");
        getEndpointLocator().getDomainEndpoint().saveYaml(new DataYamlSaveFasterXmlRequest(getToken()));
    }

}