package ru.t1.dkozoriz.tm.command.data.save;

import lombok.SneakyThrows;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataJsonLoadJaxBRequest;
import ru.t1.dkozoriz.tm.dto.request.data.save.DataBase64SaveRequest;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    public DataBase64SaveCommand() {
        super("data-save-base64", "Save data to base64 file.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        getEndpointLocator().getDomainEndpoint().saveBase64(new DataBase64SaveRequest(getToken()));
    }

}