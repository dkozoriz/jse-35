package ru.t1.dkozoriz.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    public UserChangePasswordCommand() {
        super("change-user-password", "change password of current user.");
    }

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        getEndpointLocator().getUserEndpoint()
                .userChangePassword(new UserChangePasswordRequest(getToken(), password));
    }

}