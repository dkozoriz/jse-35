package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.repository.ISessionRepository;
import ru.t1.dkozoriz.tm.api.service.ISessionService;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.repository.SessionRepository;

public final class SessionService extends UserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository sessionRepository) {
        super(sessionRepository);
    }

    @Override
    protected @NotNull String getName() {
        return "Session";
    }

}