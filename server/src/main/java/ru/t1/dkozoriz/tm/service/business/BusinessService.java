package ru.t1.dkozoriz.tm.service.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.business.IBusinessRepository;
import ru.t1.dkozoriz.tm.api.service.business.IBusinessService;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.IndexIncorrectException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.exception.field.StatusEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;
import ru.t1.dkozoriz.tm.service.UserOwnedService;

import java.util.List;

public abstract class BusinessService<T extends BusinessModel, R extends IBusinessRepository<T>>
        extends UserOwnedService<T, R> implements IBusinessService<T> {

    public BusinessService(@NotNull final R abstractRepository) {
        super(abstractRepository);
    }

    @NotNull
    public T add(@Nullable final String userId, @Nullable final T model) {
        if (model == null) throw new EntityException(getName());
        if (userId == null) throw new UserIdEmptyException();
        model.setUserId(userId);
        return repository.add(model);
    }

    @NotNull
    public T changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final T model = findById(userId, id);
        if (model == null) throw new EntityException(getName());
        model.setStatus(status);
        return model;
    }

    @NotNull
    public T changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final T model = findByIndex(userId, index);
        if (model == null) throw new EntityException(getName());
        model.setStatus(status);
        return model;
    }

    @NotNull
    public T findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final T model = repository.findByIndex(userId, index);
        if (model == null) throw new EntityException(getName());
        return model;
    }

    @Nullable
    public List<T> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort.getComparator());
    }


    @Nullable
    public T removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

    @NotNull
    public T updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final T model = findById(userId, id);
        if (model == null) throw new EntityException(getName());
        model.setName(name);
        model.setDescription(description);
        return model;
    }

    @NotNull
    public T updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final T model = findByIndex(userId, index);
        model.setName(name);
        model.setDescription(description);
        return model;
    }

}