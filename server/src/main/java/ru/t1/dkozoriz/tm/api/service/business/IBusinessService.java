package ru.t1.dkozoriz.tm.api.service.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.service.IUserOwnedService;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;

import java.util.List;

public interface IBusinessService<T extends BusinessModel> extends IUserOwnedService<T> {

    @Nullable
    T removeByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    T create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    List<T> findAll(@Nullable String userId, @Nullable Sort comparator);

    @NotNull
    T updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    T updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    T findByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    T changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    T changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}