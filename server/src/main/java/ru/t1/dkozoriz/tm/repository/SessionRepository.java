package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.api.repository.ISessionRepository;
import ru.t1.dkozoriz.tm.model.Session;

public final class SessionRepository extends UserOwnedRepository<Session> implements ISessionRepository {

}